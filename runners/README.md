Note that a single machine can use multiple executors :)

# NixOS with Docker executor

Put the following snippets in `configuration.nix`:

```nix
virtualisation = {
  docker = {
    enable = true;
  };
};
```

```nix
services = {
  gitlab-runner = {
    enable = true;
    configFile = /etc/gitlab-runner/config.toml;
  };
};
```
Now [register the runner](https://docs.gitlab.com/runner/register/). It
may be wise to double check the contents of `/etc/gitlab-runner/config.toml`.

If your machine is fast (i5-7200U or better), consider registering it with the `intense` tag.
Note that a single machine is not restricted to a single tag.

# NixOS with Virtualbox executor

Put the following snippet in `configuration.nix`:

```nix
virtualisation = {
  virtualbox.host = {
    enable = true;
    headless = true;  # Optional
  };
};
```

```nix
services = {
    gitlab-runner = {
      enable = true;
      configFile = /etc/gitlab-runner/config.toml;
      packages = with pkgs; [
        bash
        docker-machine
        (writers.writeDashBin "vboxmanage" ''
          ${virtualbox}/bin/VBoxManage "$@"
        '')
        ];
    };
};
```

Additionally:

1. `# vagrant up --provider=virtualbox` (NB, as root) on a Vagrantfile in a directory named debian-<version>-virtualbox of your choice.

2. `# vagrant halt` turn off VM as the gitlab-runner user.

3.  Make sure the correct virtualbox name is set in `/etc/gitlab-runner/config.toml`

```toml
  [runners.virtualbox]
    base_name = "aioli-vm-debian-<version>-nixos-<version>"
```

4. `# nixos-rebuild switch`


Now [register the runner](https://docs.gitlab.com/runner/register/) with the tag `vm-debian-<version>-nixos-<version>`. It
may be wise to double check the contents of `/etc/gitlab-runner/config.toml`.

# CI tags

`vm-<distro>-<version>-nixos-<version>` = jobs that need a virtual machine.

`intense` = jobs that are computationally intense and would be slow if not ran on on a fast runner.

# Troubleshooting

## Pinned core starting virtualbox vm

If the virtualbox network is weirdly configured, a core can sometimes be pinned trying to start a hanging vm.

1. Kill the proccess

`$ sudo kill <pid>`

2. Delete the vm

`$ sudo VBoxManage list vms`

`$ sudo VBoxManage unregistervm --delete <uid>`
