#!/bin/sh

# This script generates output in the following form.
# "--env <key1>=<value1> --env <key2>=<value2> --env <keyN>=<valueN>"
# This output is passed to `$ gitlab-runner exec docker`, for instance:
# `$ gitlab-runner exec docker $(runner-env.sh) <job>`

# It reguires the following.
# - lci_reg_user and lci_reg_pass to set.

# WARNING: currently hardcoded for aioli usage, change as needed.

CI_REGISTRY="registry.gitlab.com"
CI_REGISTRY_IMAGE="$CI_REGISTRY/fricloud/aioli-infra"

# echo each line with substition as opposed to cat
# prefix with " --env"
# remove the initial space
xargs echo << EOF | xargs -n 1 echo -n " --env" | cut -c 2-
CI_REGISTRY=$CI_REGISTRY
CI_REGISTRY_USER=$lci_reg_user
CI_REGISTRY_PASSWORD=$lci_reg_pass
CI_REGISTRY_IMAGE=$CI_REGISTRY_IMAGE
EOF
