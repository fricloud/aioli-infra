#!/bin/sh
# shellcheck disable=SC2068
# ^ Intentionally unquoted arugments

permission() {
  # "Server" is only present if we have docker permissions
  if docker version 2> /dev/null | grep -q Server > /dev/null 2>&1; then
    return 0
  else
    return 1
  fi
}

# if nix installed
if command -v nix > /dev/null 2>&1; then
  if permission; then
    $@
  elif command -v direnv > /dev/null 2>&1; then
    if ! sudo direnv exec . $@; then
      printf "[As root user]"
    fi
  else
    echo "direnv not found and no permission to docker"
    exit 1
  fi
else
  if permission; then
    $@
  else
    if ! sudo $@; then
      printf "[As root user]"
    fi
  fi
fi
