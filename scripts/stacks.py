#!/usr/bin/env python3
import docker
import vars as v
import argparse
import subprocess as s
import os.path

NOT_FOUND = "{} not found. {}"
VARS_APPLY = "Have you tried running `aioli vars apply`?"

def prompt(choices, default, message):
    for i, choice in enumerate(choices):
        print(f'{i}. {choice}')
    choice = input(f'{message} [default={default}]: ')
    if choice != '':
        try:
            result = choices[int(choice)]
        except ValueError:
            print("Please enter a valid value.")
            result = prompt(choices, default, message)
    else:
        result = choices[default]
    return result


def init_swarm(client, advertise_addr=None):
    try:
        if advertise_addr != None:
            client.swarm.init(advertise_addr)
        client.swarm.init()
    except docker.errors.APIError as e:
        resp = str(e)
        if "already part of a swarm" in resp:
            pass
        elif "multiple addresses on different interfaces" in resp:
            # ip addresses and interfaces
            ipsAndIfs = [item.strip() for item in resp.split('(')[-1].rstrip(')")').split('and')]
            m = 'Select swarm advertise address'
            addr = prompt(ipsAndIfs, 0, m).split()[0]
            client.swarm.init(advertise_addr=addr)
        else:
            raise


def init_networks(client):
    def create_network(n, *args, **kwargs):
        try:
            client.networks.create(n, *args, **kwargs)
        except docker.errors.APIError as e:
            resp = str(e)
            if "already exists" in resp:
                # No duplicate overlay networks allowed, therefore
                # always returns a list with only one network
                existing = client.networks.list(names=[f'{n}'])[0]
                # Remove old, add new (params could have changed)
                existing.remove()
                client.networks.create(n, *args, **kwargs)
            else:
                raise
    # swarm scope is default
    allVars = v.fetch_vars("group_vars")
    for service in allVars['srv']:
        # balance traffic is already encrypted using TLS
        create_network(f"balance_{service}", driver="overlay")

        # non-TLS LDAP traffic is insecure so IPsec is used.
        # This is simpler than setting up TLS.
        create_network(f"ldap_{service}", driver="overlay", options={"encrypted": ""})

        # We don't have single sign on yet
        # create_network(f"sso_{service}", driver="overlay")


def clean(client):
    # Leaving swarm will remove overlay networks in swarm scope
    client.swarm.leave(force=True)


def start_stack(name, dirpath, docker_path):
   path = os.path.join(dirpath, "build/stack.yml")
   if os.path.isfile(path):
       proc = s.run([docker_path] + "stack deploy -c".split() + [path, name])
       if proc.returncode != 0:
           raise Exception(f"{' '.join(proc.args)} returned non-0 return code: {proc.returncode}.")
   else:
       raise Exception(NOT_FOUND.format(str(path), VARS_APPLY))


def start_stacks(docker_path):
    def middlefunc(name, dirpath):
        return start_stack(name, dirpath, docker_path)
    v.for_stack('stacks', middlefunc)


def stop_stack(name, dirpath, docker_path):
    proc = s.run([docker_path] + "stack rm".split() + [name])
    if proc.returncode != 0:
        raise Exception(f"{' '.join(proc.args)} returned non-0 return code: {proc.returncode}.")


def stop_stacks(docker_path):
    def middlefunc(name, dirpath):
        return stop_stack(name, dirpath, docker_path)
    v.for_stack('stacks', middlefunc)

if __name__ == "__main__":
    cli = argparse.ArgumentParser(description="manages docker stacks")
    cli.add_argument("--docker-path", help="Path to docker executable", default="docker")


    subcli = cli.add_subparsers(dest="action")

    cli_up = subcli.add_parser("up", help="inits a swarm, creates networks and brings up all stacks.")
    cli_up.add_argument("-s", "--stack-name", help="Bring up only a specific stack.")
    cli_up.add_argument("-a", "--advertise-addr", help="Advertised address (format: <ip|interface>[:port])")

    cli_down = subcli.add_parser("down", help="brings down")
    cli_down.add_argument("-s", "--stack-name", help="Bring down only a specific stack.")

    subcli.add_parser("clean", help="leaves the swarm removing all networks and bringing down all stacks")

    args = cli.parse_args()

    client = docker.from_env()

    #print(args.stack_name)
    #print(type(args.stack_name))

    if args.action == "clean":
        clean(client)
    elif args.action == "down":
        if args.stack_name == None:
            stop_stacks(args.docker_path)
        else:
            dirpath = os.path.join("stacks/", args.stack_name)
            stop_stack(args.stack_name, dirpath, args.docker_path)
    elif args.action == "up":
        init_swarm(client)
        init_networks(client)
        if args.stack_name == None:
            start_stacks(args.docker_path)
        else:
            dirpath = os.path.join("stacks/", args.stack_name)
            start_stack(args.stack_name, dirpath, args.docker_path)
    else:
        cli.print_help()
