#!/usr/bin/env python3
import os
import sys
import os.path
import shutil
from pathlib import Path
import yaml
from jinja2 import Environment
import string
import secrets

# Local import
import env

def gen_pass():
  charset = string.ascii_letters + string.digits
  password = ''.join(secrets.choice(charset) for i in range(64))
  return password

def for_stack(stackroot, f, stacks=[]):
    """
    Call f for every stack
    """
    # If no stacks are given lust loop the dir
    if len(stacks) == 0:
        stacks = []
        for fname in os.listdir(stackroot):
            folder = os.path.join(stackroot, fname)
            if os.path.isdir(folder):
                stacks.append(fname)

    for stack in stacks:
        folder = os.path.join(stackroot, stack)
        if not os.path.isdir(folder):
            sys.exit(f"folder {folder} could not be found")
        f(stack, folder)

def fetch_vars(configdir):
    """
    Returns all variables in a dict.
    Test vars will have precedence over prod vars.
    """
    allVars = {}
    varFiles = Path(configdir).rglob('*.yml')
    testFiles = []
    for varFile in varFiles:
        if "test" in str(varFile):
            testFiles.append(varFile)
        else:
            currFile = open(varFile, 'r')
            allVars.update(yaml.safe_load(currFile))
            currFile.close()
    # use test vars if test
    if env.is_test():
        for testFile in testFiles:
            currFile = open(testFile, 'r')
            allVars.update(yaml.safe_load(currFile))
            currFile.close()
    return allVars

def substdir(stackpath, config, outdir="build", force=False):
    """
    Substitutes the supplied variables in all supplied files.
    and places them in a build directory of which the path is returned.
    This path is also available when templating as outDir.

    :param files: filenames relative to stackpath
    :param outdir: where to put templated files, relative to stackpath
    """
    outdir = os.path.join(stackpath, outdir)

    env = Environment()
    env.globals["gen_pass"] = gen_pass

    # Runs for every folder recursively
    for folder, _, files in os.walk(stackpath):
        # Lets not do something stupid
        if folder == outdir:
            continue

        for fname in files:
            fpath = os.path.join(stackpath, fname)

            subResult = open(fpath, 'r').read()
            count = 0

            # Keep going until all variables are substituted
            while True:
              prevSubResult = subResult
              subResult = env.from_string(subResult).render(config)

              count += 1
              # if rendering again does nothing, it must mean all variables are substituted
              if prevSubResult == subResult:
               break

            buildpath = os.path.join(outdir, fname)

            # Make parent directories
            os.makedirs(os.path.dirname(buildpath), exist_ok=True)

            if os.path.isfile(buildpath) and not force:
                sys.exit(f"file {buildpath} already exists")

            with open(buildpath, "w") as f:
                f.write(subResult)

    return outdir

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description="tools to handle variable templating")

    parser.add_argument("--stack-dir", help="overwrite stacks folder path relative to root", default="stacks")
    parser.add_argument("--config-path", "-c", help="where to pull vars from, relative to root", default="group_vars")

    subparser = parser.add_subparsers(dest="action")
    cleanparser = subparser.add_parser("clean", help="cleanup applied vars")

    applyparser = subparser.add_parser("apply", help="apply vars")
    applyparser.add_argument("stack", default=[], nargs='?', help="which stack to apply vars to")
    applyparser.add_argument("--keep", action="store_true", help="do not overwrite existing files")

    args = parser.parse_args()

    if args.action == "clean":
        def clean(name, stack_dir):
            # Well this just deletes some stuff
            folder = os.path.join(stack_dir, "build")
            if os.path.exists(folder):
                shutil.rmtree(folder)
        for_stack(args.stack_dir, clean)
    elif args.action == "apply":
        def apply(name, stack_dir):
            v = fetch_vars(args.config_path)

            # v = ins_passwords(v)
            outdir = substdir(stack_dir, v, force=(not args.keep))
            print(outdir)

        for_stack(args.stack_dir, apply, stacks=args.stack)
    else:
        parser.print_help()
