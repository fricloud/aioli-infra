# Scripts

This is where various scripts are kept.

## check.py
Runs syntax checks and linters.
This includes yamllint and ansible-lint.

## stacks.py

Handles docker stacks.
This includes creating external networks for services to connect to.

Currently, for each `srv:` in variables, the following networks are created:

- balance_
For the connection between the given service and haproxy.
It is assumed that this traffic in secured (usually by TLS).

- ldap_
For the conection between the given service and ldap.
It is assumed that this traffic is insecure (usually "missing" TLS).
Thus, it is secured by making it an [encrypted overlay network](https://docs.docker.com/network/overlay/#encrypt-traffic-on-an-overlay-network).
