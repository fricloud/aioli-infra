#!/usr/bin/env python3

import argparse
import subprocess as s
import sys
import os

def do_all():
    s.run("./aioli vars apply".split())
    s.run("yamllint -c .yamllint.yml .".split())
    s.run("./aioli vars clean".split())
    s.run("ansible-lint playbook.yml".split())
    s.run("ansible-playbook playbook.yml --syntax-check".split())

if __name__ == "__main__":
    cli = argparse.ArgumentParser(description="Runs syntax checks and linters.")
    subcli = cli.add_subparsers(dest="action")

    subcli.add_parser("all", help="runs all syntax checks and linters.")

    args = cli.parse_args()

    if args.action == "all":
        do_all()
    else:
        cli.print_help()
