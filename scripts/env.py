from enum import Enum
import os

class Env(Enum):
    DEV = "DEV"
    TEST = "TEST"
    PROD = "PROD"

def is_dev():
    return get() is Env.DEV

def is_test():
    return get() is Env.TEST

def is_prod():
    return get() is Env.PROD

def get():
    """
    Returns the current env
    """
    # Default to Env.DEV
    if not "AIOLI_ENV" in os.environ:
        return Env.DEV

    env = Env(os.environ["AIOLI_ENV"])

    return env
